# Laptop Service

from flask import Flask
import flask.scaffold
flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient
import os
import logging
import json
import arrow

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient("db", 27017)
db = client.timesdb
collection = db['timesdb']



class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }
# default option is json
class listAll(Resource):
    def get(self):
        log.debug("Did we get to listAll?")
        
        # Get all documents in a collection AND exclude the _id field
        _items = collection.find({},{'_id': False})

        items = [item for item in _items]
        # items = json.dumps(_items)
        log.debug(items)
        # print(_items)

        return{'Brevets': items

        }

class listOpens(Resource):
    def get(self):

        parser = reqparse.RequestParser()
        parser.add_argument('top', type=int)

        query_args = parser.parse_args()

        if query_args.get('top'):

            _items = collection.find({},{'_id': False, 'controls.open': True, 'begin_date': True, "begin_time": True})
            selected_items = [item for item in _items]
            items = []
            print(selected_items)
            for thing in selected_items:
                for opentime in thing["controls"]:

                    items.append(arrow.get(opentime['open'], "ddd M/D/YYYY HH:mm"))

            items = sorted(items)
            top_what = query_args.get('top')
            items = items[:top_what]
        
        else:
            # Get all documents in a collection AND exclude the _id field
            _items = collection.find({},{'_id': False, 'controls.open': True, 'begin_date': True, "begin_time": True})

            items = [item for item in _items]

        return{'Brevets': items

        }


class listClose(Resource):
    def get(self):
        # Get all documents in a collection AND exclude the _id field
        _items = collection.find({},{'_id': False, 'controls.close': True, 'begin_date': True, "begin_time": True})

        items = [item for item in _items]

        return{'Brevets': items

        }

class topK(Resource):
    def get(self):
        # Get all documents in a collection AND exclude the _id field
        _items = collection.find({},{'_id': False, 'controls.close': True, 'begin_date': True, "begin_time": True})

        items = [item for item in _items]

        return{'Brevets': items

        }

# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll', '/listAll/json')
api.add_resource(listOpens, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listClose, '/listCloseOnly', '/listCloseOnly/json')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)

